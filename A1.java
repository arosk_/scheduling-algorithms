/**
 * COMP2240 - OPERATING SYSTEMS (SEM 2, 2019)
 *
 * @author: SORA KHAN - 3197198
 * 18 Aug 2019 -
 *
 */
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
public class A1
{
    public static void main (String[] args)
    {
        if (args.length == 0) {
            System.out.println("java A1 <filename>.txt");
            System.exit(0);
        }

        A1 intFace = new A1 ();
        intFace.run(args[0]);
    }

    private void run (String fileName)
    {
        ArrayList<Process> dispatcher = new ArrayList<>(); // list of jobs = processes to be run!

        try {
            File file = new File(fileName);
            BufferedReader br = new BufferedReader(new FileReader(file));

            String line;
            String id = "";
            int dispTime = 0, arrivalTime = 0, serviceTime = 0;
            boolean foundProcess = false;
            while ((line = br.readLine()) != null) { // goes through a line
                if (line.indexOf("DISP") == 0) dispTime = Integer.parseInt(line.split(" ")[1]);
                else if (line.indexOf("ID") == 0) id = line.split(" ")[1];
                else if (line.indexOf("Arrive") == 0) arrivalTime = Integer.parseInt(line.split(" ")[1]);
                else if (line.indexOf("ExecSize") == 0) {
                    serviceTime = Integer.parseInt(line.split(" ")[1]);
                    foundProcess = true;
                } // once ExecSize has been read, a process is found, therefore added to process list, and then to start looking for anothe process
                if (foundProcess) dispatcher.add(new Process(id, arrivalTime, serviceTime));
                foundProcess = false;
            }
            sortByID(dispatcher); // sort the processes in the dispatcher in such a way that the ones with the ID's say Px and Py, Px comes before Py
            int start = 0, quantumTime = 0;
            int end = calcFinishTime(dispatcher);
            Scheduler fcfs = new FirstComeFirstServe (dispatcher, dispTime); // processes read from file gets passed on!
            Scheduler roundRobin = new RoundRobin (dispatcher, dispTime);
            Scheduler feedback = new Feedback (dispatcher, dispTime);
            Scheduler narrowRoundRobin = new NarrowRoundRobin (dispatcher, dispTime);

            Scheduler[] schedulers = {fcfs, roundRobin, feedback, narrowRoundRobin};
            System.out.println("end: " + end);
            scheduleProcesses(schedulers,end,dispTime);
            summarize(schedulers);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
    // This method runs through every scheduler and their processes and displays the turnaround and waiting times
    private void scheduleProcesses (Scheduler[] schedulers, int endTime, int dispTime)
    {
        for (int i = 0 ; i < schedulers.length ; i++) {
            int start = 0, quantumTime = 0;
            while (start < endTime) { // the time a process finishes processing is retrieved,
                quantumTime = schedulers[i].run(quantumTime,dispTime); // and the next process will start from the new quantumTime (retrieved finish time + dispatcher time)
                start++;
                // System.out.println(start);
            } System.out.println(schedulers[i]); // toString of the Scheduler, which gets the turnaround and waiting times of each process under each Schduler type
        }
    }

    // This method gathers the average waiting and turnaround times for each Scheduler
    private void summarize (Scheduler[] schedulers)
    {
        System.out.println(String.format("Summary\n%-20s%-30s%-30s",  "Algorithm",  "Average Turnaround Time" ,  "Average Waiting Time"));
        for (int i = 0 ; i < schedulers.length ; i++)
            System.out.println(schedulers[i].generateSummary());
    }
    // This method calculates the total time for how long the service of scheduling should run
    // this is done by adding up the total service times for each processes
    private int calcFinishTime (ArrayList<Process> processTimes)
    {
        int total = 0;
        for (Process p : processTimes) total += p.getServiceTime();
        return total;
    }

    // this method checks if any of the processes have the same attribute values (other than the processID)
    // and sorts them by ID Px comes before Py
    private void sortByID (ArrayList<Process> dispatcher)
    {
        for (int i = 0 ; i < dispatcher.size() ; i++) {
            String iID = dispatcher.get(i).getProcessID();
            int iArrival = dispatcher.get(i).getArrivalTime();
            int iService = dispatcher.get(i).getServiceTime();
            for (int j = i+1 ; j < dispatcher.size() ; j++) {
                String jID = dispatcher.get(j).getProcessID();
                int jArrival = dispatcher.get(j).getArrivalTime();
                int jService = dispatcher.get(j).getServiceTime();
                if (iArrival == jArrival && iService == jService)
                    if (iID.compareTo(jID) > 0)
                        Collections.swap(dispatcher,i,j);
            }
        }
    }
}
