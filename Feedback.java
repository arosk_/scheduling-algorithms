// 29 Aug 2019 - Sora Khan - 3197198
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
public class Feedback extends Scheduler
{
    private final int MILLISEC = 4;
    private ArrayList<Process> processCopy = new ArrayList<>(); // prevents original processes from being changed!!
    private Queue<Process>[] priorityQueues = new Queue[6]; // 6 because 0-5 priority as required!, the indices represent the priorities!
    private int currentQueueNum = 0;
    protected Feedback (ArrayList<Process> processes, int dispatcherTime)
    {
        super(processes); // stores all processes read from file - to the list of processes!
        for (Process p : processes) processCopy.add(new Process(p)); // to preserve original service time
        // each element will consist of a queue! and the indices represent the priority
        for (int i = 0 ; i < 6 ; i++) priorityQueues[i] = new LinkedList<Process>();
    }

    @Override
    protected int run (int currentTime, int actualDispTime)
    {
        if (!populated) {
            super.populateQueues(currentTime, processCopy);
            // populate Q01 with whatever is in ready queue
            for (Process p : readyQueue) // populate the first 0-priority queue with the ready ones
                priorityQueues[0].add(p);
        } else {
            boolean usualProcessing = true;
            int currentQueueNum = 0;

            // checks which priotity queue needs to be tackled
            for (int i = 0 ; i < priorityQueues.length ; i++) {
                // if the one with highest priority (one with lowest number) is found to have items, that's the queue we have to process first ..
                if (priorityQueues[i].size() != 0) {    // basically want to clear out queues/indices from left to right
                    currentQueueNum = i;
                    break;
                }
            }
            // this is just so that if a process is the only process to be processed but another one arrives way after
            if (priorityQueues[0].size() == 0 && blockedQueue.size() != 0) {
                priorityQueues[0].add(blockedQueue.remove());
            }

            if (priorityQueues[currentQueueNum].size() != 0) {
                int time = 0, end = 0;
                time = currentTime + actualDispTime;  // local times. Independent to actual current time
                end = time + MILLISEC;
                Process current = priorityQueues[currentQueueNum].remove();
                while (time < end) { // this is to check if any other process in the blocked queue should be added to the ready queue while a process is being processed
                    time++;
                    if (blockedQueue.size() != 0) { // if there are processes blocked
                        if (time >= blockedQueue.peek().getArrivalTime()) // check if the simulated current time has passed the arrival times of a process
                            priorityQueues[0].add(blockedQueue.remove()); // if so, we need to add this process to the readyQueue since it is ready for processing
                        else if (priorityQueues[0].size() == 0) break; // only if there is nothing in the high priority queue, check next priority queue
                    } else break; // no more blocked processes
                }

                // nothing is in the ready queue and you have to wait .. till the next process arrives.
                if (readyQueue.size() == 0 && blockedQueue.size() != 0) {
                    readyQueue.add(blockedQueue.remove());
                }

                if (current.getServiceTime() > MILLISEC ) {
                    current.setServiceTime(current.getServiceTime() - MILLISEC); // add new service time to current process
                    startTime = currentTime + actualDispTime;
                    finishTime = startTime + MILLISEC; // finish times end according to the millisecs
                    priorityQueues[currentQueueNum+1].add(current); // move process to next queue
                    // if (priorityQueues[currentQueueNum+1].size() != 0) usualProcessing = false;
                    for (int i = 0 ; i < priorityQueues.length ; i++) {
                        // if the one with highest priority (one with lowest number) is found to have items, that's the queue we have to process first ..
                        if (priorityQueues[i].size() != 0)     // basically want to clear out queues/indices from left to right
                            usualProcessing = false;
                    }
                }

                if (usualProcessing) { // if either the service time is < millisec or no more processes waiting in ready queue
                    startTime = finishTime + actualDispTime;
                    finishTime = startTime + current.getServiceTime();
                }
                current.setFinishTime(finishTime); ////
                calcWatingAndTurnaroundTimes(current, getOriginalServiceTime(current));
            }
        }
        return finishTime;
    }

    public String toString ()
    {
        return generateSchedulingTimes("FB (constant)",processCopy);
    }

    @Override
    protected String generateSummary ()
    {
        return String.format("%-20s%-30s%-30s",  "FB (constant)",  calcAvgTurnaroundTime(processCopy) ,  calcAvgWaitingTime(processCopy));
    }
}
