// Sora Khan - 3197198
import java.util.ArrayList;
public class FirstComeFirstServe extends Scheduler
{
    private ArrayList<Process> processCopy = new ArrayList<>(); // prevents original processes from being changed!!

    public FirstComeFirstServe (ArrayList<Process> processes, int dispatcherTime)
    {
        // super(processes) is needed because Scheduler gets re-initialised for each different Scheduler
        // without this, we are directly changing the processList ArrayList for other Schedulers
        super(processes); // stores all processes read from file - to the list of processes!
        for (Process p : processes) processCopy.add(new Process(p)); // to preserve original service time
    }

    @Override
    protected int run (int currentTime, int actualDispTime)
    {
        if (!populated) super.populateQueues(currentTime, processCopy);
        else {
            Process current = new Process();

            // if the queues have been populated and there are processes that need to be processed
            if (readyQueue.size() != 0) {
                int time = 0, end = 0;
                current = readyQueue.remove();
                time = currentTime + actualDispTime;  // local times. Independent to actual current time
                end = time + current.getServiceTime();

                // calcFinishTime(processCopy) here is for the dispatcher to wait on for other processes to be processed ..
                // because of the total service time, it knows that it should wait for other processes
                while (time < calcFinishTime(processList)) { // this is to check if any other process in the blocked queue should be added to the ready queue while a process is being processed
                    time++;
                    if (blockedQueue.size() != 0) { // if there are processes blocked
                        if (time >= blockedQueue.peek().getArrivalTime()){ // check if the simulated current time has passed the arrival times of a process
                            readyQueue.add(blockedQueue.remove()); // if so, we need to add this process to the readyQueue since it is ready for processing
                            break;
                        }
                    } else break; // no more blocked processes
                }

                // nothing is in the ready queue and you have to wait .. till the next process arrives.
                if (readyQueue.size() == 0 && blockedQueue.size() != 0) {
                    readyQueue.add(blockedQueue.remove());
                }

                // usualProcessing
                startTime = finishTime + actualDispTime;

                if (current.getArrivalTime() > currentTime)  // starts at the correct time - if arrival time is way after the first process finished
                    startTime = current.getArrivalTime();

                finishTime = startTime + current.getServiceTime();




                current.setFinishTime(finishTime); ////

                calcWatingAndTurnaroundTimes(current, current.getServiceTime());



            }
        }   return finishTime; // returning this so the next process can start at the next time (after dispatcher)
    }


    public String toString ()
    {
        return generateSchedulingTimes("FCFS",processCopy);
    }

    @Override
    protected String generateSummary ()
    {
        return String.format("%-20s%-30s%-30s",  "FCFS",  calcAvgTurnaroundTime(processCopy) ,  calcAvgWaitingTime(processCopy));
    }
}
