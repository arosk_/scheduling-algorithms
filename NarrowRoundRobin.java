// Sora Khan - 3197198
import java.util.ArrayList;
public class NarrowRoundRobin extends RoundRobin
{

    private ArrayList<Process> processCopy = new ArrayList<>(); // prevents original processes from being changed!!
    private ProcessQuantum[] processQuantums; // holds all processes read from file - which will be initialised to have a millisec/quantum time of 4

    protected NarrowRoundRobin (ArrayList<Process> processes, int dispatcherTime)
    {
        super(processes, dispatcherTime); // stores all processes read from file - to the list of processes!
        int numOfProcesses = processes.size();
        processQuantums = new ProcessQuantum[numOfProcesses];
        int i = 0;
        for (Process p : processes) {
            processCopy.add(new Process(p)); // to preserve original service time
            // every process read from a file will be stored with a quantum time of 4 ms
            processQuantums[i] = new ProcessQuantum(p,MILLISEC);
            i++;
        }
    }

    @Override
    protected int run (int currentTime, int actualDispTime)
    {
        if (!populated) {
            super.populateQueues(currentTime, processCopy);
        } else {
            Process current = new Process();
            boolean usualProcessing = true;

            // if the queues have been populated and there are processes that need to be processed
            if (readyQueue.size() != 0) {

                current = readyQueue.remove();
                ProcessQuantum pq = findProcessQuantum (current);

                int time = 0, end = 0;
                time = currentTime + actualDispTime;  // local times. Independent to actual current time
                end = time + pq.quantum;

                while (time < end) { // this is to check if any other process in the blocked queue should be added to the ready queue while a process is being processed
                    time++;
                    if (blockedQueue.size() != 0) { // if there are processes blocked
                        if (time >= blockedQueue.peek().getArrivalTime()) // check if the simulated current time has passed the arrival times of a process
                            readyQueue.add(blockedQueue.remove()); // if so, we need to add this process to the readyQueue since it is ready for processing
                    } else break; // no more blocked processes
                }
                // nothing is in the ready queue and you have to wait .. till the next process arrives. 
                if (readyQueue.size() == 0 && blockedQueue.size() != 0) {
                    readyQueue.add(blockedQueue.remove());
                }
                // processing - based on millsecs
                if (current.getServiceTime() > pq.quantum && readyQueue.size() != 0) { // if a process has a service time > millsecs
                    // check if there are other processes in the ready queue.
                    current.setServiceTime(current.getServiceTime() - pq.quantum); // add new service time to current process
                    readyQueue.add(current); // add current process BACK to queue - to process since it hasn't finished processing yet
                    startTime = currentTime + actualDispTime;
                    finishTime = startTime + pq.quantum; // finish times end according to the millisecs

                    usualProcessing = false; // since there are other processes in the readyQueue, we will keep going by millsecs
                    if (pq.quantum != 2) pq.quantum--;
                }
                // to process as normal, without any millisec check etc.
                // this is good for processes that have a service time > millisec, but is the only process left in the queue, so we need to process them anyway
                if (usualProcessing) { // if either the service time is < millisec or no more processes waiting in ready queue
                    startTime = finishTime + actualDispTime;
                    finishTime = startTime + current.getServiceTime();
                }
                current.setFinishTime(finishTime); ////
                calcWatingAndTurnaroundTimes(current, getOriginalServiceTime(current));
            }
        }

        return finishTime; // returning this so the next process can start at the next time (after dispatcher)
    }

    // Gets the correct process to be able to calculate with its own quantum time!
    private ProcessQuantum findProcessQuantum (Process p)
    {
        ProcessQuantum pq = null;
        for (int i = 0 ; i < processQuantums.length ; i++) {
            if (p.getProcessID().equals(processQuantums[i].process.getProcessID()))
                pq = processQuantums[i];
        }
        return pq;
    }
    // This class associates a Process with its own quantum number!
    private class ProcessQuantum
    {
        Process process;
        int quantum;
        ProcessQuantum (Process p, int q)
        {
            process = p;
            quantum = q;
        }
    }

    public String toString ()
    {
        return generateSchedulingTimes("NRR",processCopy);
    }

    @Override
    protected String generateSummary ()
    {
        return String.format("%-20s%-30s%-30s",  "NRR",  calcAvgTurnaroundTime(processCopy) ,  calcAvgWaitingTime(processCopy));
    }
}
