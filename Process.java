// Sora Khan - 3197198
public class Process
{
    private String processID;
    private int arrivalTime;
    private int serviceTime;
    private int turnaroundTime;
    private int waitingTime;
    private int finishTime;

    public Process ()
    {
        this.processID = "";
        this.arrivalTime = 0;
        this.serviceTime = 0;
        this.turnaroundTime = 0;
        this.waitingTime = 0;
        this.finishTime = 0;
    }

    public Process (Process p)
    {
        this.processID = p.getProcessID();
        this.arrivalTime = p.getArrivalTime();
        this.serviceTime = p.getServiceTime();
    }

    public Process (String processID, int arrivalTime, int serviceTime)
    {
        this.processID = processID;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
    }

    public String toString ()
    {
        return "ID: " + this.processID + "\nArrive: " + this.arrivalTime + "\nExecSize: " + this.serviceTime;
    }

    public void setProcessID (String processID) { this.processID = processID; }
    public void setArrivalTime (int arrivalTime) {  this.arrivalTime = arrivalTime; }
    public void setServiceTime (int serviceTime) { this.serviceTime = serviceTime; }
    public void setTurnaroundTime (int turnaroundTime) { this.turnaroundTime = turnaroundTime; }
    public void setWaitingTime (int waitingTime) { this.waitingTime = waitingTime; }
    public void setFinishTime (int finishTime) { this.finishTime = finishTime; }

    public String getProcessID () { return this.processID; }
    public int getArrivalTime () { return this.arrivalTime; }
    public int getServiceTime () { return this.serviceTime; }
    public int getTurnaroundTime () { return this.turnaroundTime; }
    public int getWaitingTime () { return this.waitingTime; }
    public int getFinishTime () { return this.finishTime; }

}
