// Sora Khan - 3197198
import java.util.ArrayList;
public class RoundRobin extends Scheduler
{
    protected final int MILLISEC = 4;
    private ArrayList<Process> processCopy = new ArrayList<>(); // prevents original processes from being changed!!

    protected RoundRobin (ArrayList<Process> processes, int dispatcherTime)
    {
        super(processes); // stores all processes read from file - to the list of processes!
        for (Process p : processes) processCopy.add(new Process(p)); // to preserve original service time
    }

    @Override
    protected int run (int currentTime, int actualDispTime)
    {
        if (!populated) super.populateQueues(currentTime, processCopy);
        else {
            Process current = new Process();
            boolean usualProcessing = true;

            if (readyQueue.size() != 0) {
                int time = 0, end = 0;
                // if (readyQueue.peek().getArrivalTime() <= currentTime) {

                    current = readyQueue.remove();
                    time = currentTime + actualDispTime;  // local times. Independent to actual current time
                    end = time + MILLISEC;
                    // calcFinishTime(processCopy) here is for the dispatcher to wait on for other processes to be processed ..
                    // because of the total service time, it knows that it should wait for other processes
                    while (time < calcFinishTime(processList)) { // this is to check if any other process in the blocked queue should be added to the ready queue while a process is being processed
                        time++;
                        if (blockedQueue.size() != 0) { // if there are processes blocked
                            if (time >= blockedQueue.peek().getArrivalTime()){ // check if the simulated current time has passed the arrival times of a process
                                readyQueue.add(blockedQueue.remove()); // if so, we need to add this process to the readyQueue since it is ready for processing
                                break;
                            }
                        } else break; // no more blocked processes
                    }

                    // nothing is in the ready queue and you have to wait .. till the next process arrives.
                    if (readyQueue.size() == 0 && blockedQueue.size() != 0) {
                        readyQueue.add(blockedQueue.remove());
                    }

                    // processing - based on millsecs
                    // if (current.getServiceTime() > pq.quantum && readyQueue.size() != 0)
                    if (current.getServiceTime() > MILLISEC && readyQueue.size() != 0) { // if a process has a service time > millsecs
                        // check if there are other processes in the ready queue.
                        if (readyQueue.size() == 0 && blockedQueue.size() == 0) {}
                        else {
                            current.setServiceTime(current.getServiceTime() - MILLISEC); // add new service time to current process
                            readyQueue.add(current); // add current process BACK to queue - to process since it hasn't finished processing yet
                            startTime = currentTime + actualDispTime;
                            finishTime = startTime + MILLISEC; // finish times end according to the millisecs

                            if (readyQueue.size() != 0)
                                usualProcessing = false; // since there are other processes in the readyQueue, we will keep going by millsecs
                        }
                    }

                    // to process as normal, without any millisec check etc.
                    // this is good for processes that have a service time > millisec, but is the only process left in the queue, so we need to process them anyway
                    if (usualProcessing) { // if either the service time is < millisec or no more processes waiting in ready queue
                        startTime = finishTime + actualDispTime;
                        finishTime = startTime + current.getServiceTime();
                    }

                    current.setFinishTime(finishTime); ////
                    calcWatingAndTurnaroundTimes(current, getOriginalServiceTime(current));
                // }
            }
        }

        return finishTime;
    }

    public String toString ()
    {
        return generateSchedulingTimes("RR",processCopy);
    }

    @Override
    protected String generateSummary ()
    {
        return String.format("%-20s%-30s%-30s",  "RR",  calcAvgTurnaroundTime(processCopy) ,  calcAvgWaitingTime(processCopy));
    }

}
