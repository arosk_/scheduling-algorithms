// Sora Khan - 3197198
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.HashMap;
import java.util.Map;
public abstract class Scheduler
{
    protected Queue<Process> readyQueue = new LinkedList<>();
    protected Queue<Process> blockedQueue = new LinkedList<>();
    protected ArrayList<Process> processList = new ArrayList<>();
    protected int startTime;
    protected int finishTime;
    protected boolean populated = false; // gets initialised to false every time new Scheduler created

    private int waitingTime;
    private int turnaroundTime;
    private HashMap<String, String> map = new HashMap<>(); // Map to hold start time and processID
    private ArrayList<String> startTimes = new ArrayList<>(); // start times for when a process starts processing

    private Process previous = null; // helper to keep track of process previously dealt with

    public Scheduler (ArrayList<Process> processesRead)
    {
        processList = processesRead; // helper! to store all processes read from file!
    }

    protected abstract int run (int currentTime, int actualDispTime);

    protected void populateQueues (int currentTime, ArrayList<Process> processes)
    {
        // populates blocked and ready queues! whichever once necessary to be populated!
        if (readyQueue.size() == 0 && blockedQueue.size() == 0 && !populated) {
            for (Process p : processes) {
                if (p.getArrivalTime() <= currentTime) readyQueue.add(p);
                else blockedQueue.add(p);
            } populated = true;
        }
    }
    // This method calculates the Waiting and Turnaround times of a process that is being processed in a particular Scheduler at the time
    // and sets that particular process' turnaround and waiting times - within that particular scheduler's
    protected void calcWatingAndTurnaroundTimes (Process current, int serviceTime)
    {
        turnaroundTime = current.getFinishTime() - current.getArrivalTime();
        waitingTime = turnaroundTime - serviceTime;
        if (waitingTime <= 0) waitingTime = 0; // if the process didn't have to wait for too long (arrives after a process finishes) - could be 0 or -ve, so just 0
        current.setTurnaroundTime(turnaroundTime);
        current.setWaitingTime(waitingTime);

        // this is to prevent two same processes to be printed again!
        if (previous == null || previous != null && !current.getProcessID().equals(previous.getProcessID())) {
            map.put("T" + startTime, current.getProcessID()); // to associate start times with the processes!
            startTimes.add("T" + startTime); // used the start time as the key, since they are always different - yet with the same process
        }

        previous = current; // helper so can compare upcoming process with previous checked process
    }
    // This method calculates the Average Waiting time of processes that comes from the different types of Scheduler
    // passing in processCopy from the different types of schedulers !!
    // this is because the turnaroundTimes and waitingTimes differ in different schedulers
    // so need to use the list of processes from them!
    protected String calcAvgTurnaroundTime (ArrayList<Process> processFromScheduler)
    {
        double avgTurnaroundTime = 0;
        for (Process p : processFromScheduler) avgTurnaroundTime += p.getTurnaroundTime();

        avgTurnaroundTime /= processList.size();
        return String.format("%.2f", avgTurnaroundTime);
    }

    // This method calculates the Average Waiting time of processes that comes from the different types of Scheduler
    protected String calcAvgWaitingTime (ArrayList<Process> processFromScheduler)
    {
        double avgWaitingTime = 0;
        for (Process p : processFromScheduler) avgWaitingTime += p.getWaitingTime();

        avgWaitingTime /= processList.size();
        return String.format("%.2f", avgWaitingTime);
    }

    // processFromScheduler = processes with their different turnaround and waiting times from the different schedulers
    protected String generateSchedulingTimes (String schedulerType, ArrayList<Process> processFromScheduler)
    {
        String output = "\n" + schedulerType + ":\n";
        for (String times : startTimes) output += times + ": " + map.get(times) + "\n";

        output += String.format("\n%-10s%-20s%-20s\n",  "Process",  "Turnaround Time" ,  "Waiting Time");

        for (Process p : processFromScheduler)
            output += String.format("%-10s%-20s%-20s\n",  p.getProcessID(),  p.getTurnaroundTime(),  p.getWaitingTime());

        return output;
    }

    protected abstract String generateSummary ();

    // this method gets the original service time of a process, to calculate the waiting times
    protected int getOriginalServiceTime (Process current)
    {
        int serviceTime = 0;
        for (Process original : processList) // gets from original untouched list!
            if (original.getProcessID().equals(current.getProcessID()))
                serviceTime = original.getServiceTime();

        return serviceTime;
    }

    // calculates the total time for servicing the processes!
    // This is so that that Scheduler still runs since it would know that other processes still need to be processed
    protected int calcFinishTime (ArrayList<Process> processTimes)
    {
        int total = 0;
        for (Process p : processTimes) total += p.getServiceTime();
        return total;
    }
}
